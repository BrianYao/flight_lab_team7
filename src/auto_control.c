//
// auto_control:  Function to generate autonomous control PWM outputs.
// 
#define EXTERN extern
#include "../include/quadcopter_main.h"
#include "../include/PID2.h"

// Define outer loop controller 

// Outer loop controller to generate PWM signals for the Naza-M autopilot
void auto_control2(float *pose, float *set_points, int16_t* channels_ptr)
{  
  // pose (size 8):  actual {x, y , alt, yaw, xdot, ydot, altdot, yawdot}
  // set_points (size 8):  reference state (you need to set this!) 
  //                       {x, y, alt, yaw, xdot, ydot, altdot, yawdot} 

  // channels_ptr (8-element array of PWM commands to generate in this function)
  // Channels for you to set:
  // [0] = thrust
  // [1] = roll
  // [2] = pitch
  // [3] = yaw

  //x-pitch, y-roll, alt-thrust

        PID_t2* pid_pitch = PID_Init2(KP_pitch, KI_pitch, KD_pitch, roll_PWM_base);
        PID_t2* pid_roll = PID_Init2(KP_roll, KI_roll, KD_roll, roll_PWM_base);
        PID_t2* pid_thrust = PID_Init2(KP_thrust, KI_thrust, KD_thrust, thrust_PWM_base);
        PID_t2* pid_yaw = PID_Init2(KP_yaw, KI_yaw, KD_yaw, yaw_PWM_base);

        PID_SetOutputLimits2(pid_pitch, pitch_PWM_backward, pitch_PWM_forward);
        PID_SetOutputLimits2(pid_roll, roll_PWM_right, roll_PWM_left);
        PID_SetOutputLimits2(pid_thrust, thrust_PWM_down, thrust_PWM_up);
        PID_SetOutputLimits2(pid_yaw, yaw_PWM_cw, yaw_PWM_ccw);

        PID_SetIntegralLimits2(pid_pitch, -15, 15);
        PID_SetIntegralLimits2(pid_roll, -15, 15);
        PID_SetIntegralLimits2(pid_thrust, -40, 40);
        PID_SetIntegralLimits2(pid_yaw, -15, 15);

        printf("Pose: %f %f %f %f\n", pose[0], pose[1], pose[2], pose[3]);
        printf("Setpoint: %f %f %f %f\n", set_points[0], set_points[1], set_points[2], set_points[3]);

        //error
        pid_pitch->pidInput = set_points[0] - pose[0];
        pid_roll->pidInput = -(set_points[1] - pose[1]);
        pid_thrust->pidInput = -(set_points[2] - pose[2]);
        pid_yaw->pidInput = -(set_points[3] - pose[3]);
        //printf("Pitch error: %f\n", pid_pitch->pidInput);
        //printf("Roll error: %f\n", pid_roll->pidInput);
        //printf("Thrust error: %f\n", pid_thrust->pidInput);
        //printf("Yaw error: %f\n", pid_yaw->pidInput);

        // i term
        pid_thrust->ITerm = thrust_iterm;
        pid_roll->ITerm = roll_iterm;
        pid_pitch->ITerm = pitch_iterm;
        pid_yaw->ITerm = yaw_iterm; 

        // d term
        if (d_flag) 
        {
            pid_thrust->prevInput = pid_thrust->pidInput;
            pid_roll->prevInput = pid_roll->pidInput;
            pid_pitch->prevInput = pid_pitch->pidInput;
            pid_yaw->prevInput = pid_yaw->pidInput;
            d_flag = 0;
        }
        else
        {
            pid_thrust->prevInput = prev_error_thrust;
            pid_roll->prevInput = prev_error_roll;
            pid_pitch->prevInput = prev_error_pitch;
            pid_yaw->prevInput = prev_error_yaw;
        }


        PID_Compute2(pid_pitch, 3);
        PID_Compute2(pid_roll, 3);
        PID_Compute2(pid_thrust, 3);
        PID_Compute2(pid_yaw, 3);

        // d term
        prev_error_thrust = pid_thrust->prevInput;
        prev_error_roll = pid_roll->prevInput;
        prev_error_pitch = pid_pitch->prevInput;
        prev_error_yaw = pid_yaw->prevInput;
        
        // i term
        thrust_iterm = pid_thrust->ITerm;
        roll_iterm = pid_roll->ITerm;
        pitch_iterm = pid_pitch->ITerm;
        yaw_iterm = pid_yaw->ITerm;

        int Command_PWN_pitch = pid_pitch->pidOutput; 
        int Command_PWN_roll = pid_roll->pidOutput;  
        int Command_PWN_thrust = pid_thrust->pidOutput;
        int Command_PWN_yaw = pid_yaw->pidOutput;     

        printf("Pitch PWM: %d\n", Command_PWN_pitch);
        printf("Roll PWM: %d\n", Command_PWN_roll);
        printf("Thrust PWM: %d\n", Command_PWN_thrust);
        printf("Yaw PWM: %d\n", Command_PWN_yaw);

        channels_ptr[0] = Command_PWN_thrust;
        channels_ptr[1] = Command_PWN_roll;
        channels_ptr[2] = Command_PWN_pitch;
        channels_ptr[3] = Command_PWN_yaw;
        
        free(pid_pitch);
        free(pid_roll);
        free(pid_thrust);
        free(pid_yaw);
        
  return;
}

