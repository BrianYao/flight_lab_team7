#define EXTERN extern
#include "../include/quadcopter_main.h"

void * set_dynamixel(void * var){ 

servo_flag = 0;

  int i = 0;
    for(;i < 100;i++){        
        switch(servo_flag){
            case 0:
                printf("Go To Position 10 deg with 0.4 speed\n");
                printf("S: %f\n", Desire_angle);
		        pthread_mutex_lock(&dynamixel_mutex);
                bus.servo[0].cmd_angle = Desire_angle; 
                bus.servo[0].cmd_speed = 0.4; 
                bus.servo[0].cmd_flag = CMD;
                bus.servo[1].cmd_angle = Desire_angle; 
                bus.servo[1].cmd_speed = 0.4; 
                bus.servo[1].cmd_flag = CMD;
                pthread_mutex_unlock(&dynamixel_mutex);
		        servo_flag = 4;
		        break;
            case 1:
                printf("Go To Position 100 deg with 0.1 speed\n");
                pthread_mutex_lock(&dynamixel_mutex);
                bus.servo[0].cmd_angle = 100.0;
                bus.servo[0].cmd_speed = 0.1;
                bus.servo[0].cmd_flag = CMD;
                bus.servo[1].cmd_angle = 100.0;
                bus.servo[1].cmd_speed = 0.1;
                bus.servo[1].cmd_flag = CMD;
                pthread_mutex_unlock(&dynamixel_mutex);
                break;
            case 2:
                printf("Change to Wheel Mode - Stop the Servo\n");
                pthread_mutex_lock(&dynamixel_mutex);
                bus.servo[0].cmd_mode = WHEEL; 
                bus.servo[0].cmd_flag = MODE;
                bus.servo[1].cmd_mode = WHEEL; 
                bus.servo[1].cmd_flag = MODE;
                pthread_mutex_unlock(&dynamixel_mutex);
                break;
            case 3:     
                printf("Command 0.3 speed\n");
                pthread_mutex_lock(&dynamixel_mutex);
                bus.servo[0].cmd_speed = 0.3; 
                bus.servo[0].cmd_flag = CMD;
                bus.servo[1].cmd_speed = -0.3; 
                bus.servo[1].cmd_flag = CMD;
                pthread_mutex_unlock(&dynamixel_mutex);
                break;
            case 4:
                printf("Request Status:\n");
                pthread_mutex_lock(&dynamixel_mutex);
                bus.servo[0].cmd_flag = STATUS;
                bus.servo[1].cmd_flag = STATUS;
                pthread_mutex_unlock(&dynamixel_mutex);
		        servo_flag = 7;
                break;
	        case 7: 
		        pthread_mutex_lock(&dynamixel_mutex);
        	    Dynam_PrintStatus(&(bus.servo[0]));
        	    Dynam_PrintStatus(&(bus.servo[1]));
        	    pthread_mutex_unlock(&dynamixel_mutex);
		        servo_flag = 4;
            case 5:     
                printf("Command -0.3 speed\n");
                pthread_mutex_lock(&dynamixel_mutex);
                bus.servo[0].cmd_speed = -0.3; 
                bus.servo[0].cmd_flag = CMD;
                bus.servo[1].cmd_speed = 0.3; 
                bus.servo[1].cmd_flag = CMD;
                pthread_mutex_unlock(&dynamixel_mutex);
                break;
            case 6:
                printf("Change to Joint Mode - Go to default position\n");
                pthread_mutex_lock(&dynamixel_mutex);
                bus.servo[0].cmd_mode = JOINT; 
                bus.servo[0].cmd_flag = MODE;
                bus.servo[1].cmd_mode = JOINT; 
                bus.servo[1].cmd_flag = MODE;
                pthread_mutex_unlock(&dynamixel_mutex);
                break;
        }
        fflush(stdout);
        sleep(2);

        /* If we requested Status Print Status */
        //pthread_mutex_lock(&dynamixel_mutex);
        //if(servo_flag == 0) Dynam_PrintStatus(&(bus.servo[0]));
        //if(servo_flag == 0) Dynam_PrintStatus(&(bus.servo[1]));
        //pthread_mutex_unlock(&dynamixel_mutex);
    }
}
