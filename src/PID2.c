/*********************
*   PID.c
*   simple pid library 
*   pgaskell
**********************/

#include "../include/PID2.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h> 

#define MAX_OUTPUT 1.0
#define MIN_OUTPUT -1.0
#define ITERM_MIN -1.0
#define ITERM_MAX 1.0

PID_t2 * PID_Init2(float Kp, float Ki, float Kd, float trim) {
  // allocate memory for PID
  PID_t2 *pid = (PID_t2 *)malloc(sizeof(PID_t2));

  //initalize values
  pid->pidInput = 0;
  pid->pidOutput = 0;
  pid->pidSetpoint = 0;
  pid->ITerm = 0;
  pid->prevInput = 0;
  pid->trim = trim;
  pid->velocity = 0;

  // set update rate to 100000 us (0.1s)
  PID_SetUpdateRate2(pid, 100000);
  
  //set output limits, integral limits, tunings
  PID_SetOutputLimits2(pid, MIN_OUTPUT, MAX_OUTPUT);
  PID_SetIntegralLimits2(pid, ITERM_MIN, ITERM_MAX);
  PID_SetTunings2(pid, Kp, Ki, Kd);

  return pid;
}

void PID_Compute2(PID_t2* pid, int flag) {    

	float pterm = pid->kp*(pid->pidInput);

    if ( fabs(pid->pidInput) < 0.01 )
    {
        pid->ITerm = pid->ITerm;
    }
    else
    {
        pid->ITerm += pid->ki*pid->pidInput;
    }
	
    if (pid->ITerm > pid->ITermMax) {pid->ITerm = pid->ITermMax;}
    if (pid->ITerm < pid->ITermMin) {pid->ITerm = pid->ITermMin;}

    float dterm = 0;
    if (flag == 3)
    {
        dterm = pid->kd*(pid->pidInput - pid->prevInput);
    }
    else if (flag == 2)
	{
        dterm = -pid->kd2*pid->velocity;	
    }
    else
    {
        printf("no this flag\n");
    }
    
    
 
    pid->pidOutput = pterm + pid->ITerm + dterm + pid->trim;
    
	if (pid->pidOutput > pid->outputMax)
	{
	    pid->pidOutput = pid->outputMax;
	}
	if (pid->pidOutput < pid->outputMin)
	{
	    pid->pidOutput = pid->outputMin;
	}
    
    pid->prevInput = pid->pidInput;
}

void PID_SetTunings2(PID_t2* pid, float Kp, float Ki, float Kd) {
  //scale gains by update rate in seconds for proper units
  float updateRateInSec = ((float) pid->updateRate / 1000000.0);
  //set gains in PID struct
  pid->kp = Kp;
  pid->ki = Ki*updateRateInSec;
  pid->kd = Kd/updateRateInSec;
  pid->kd2 = Kd;
}

void PID_SetOutputLimits2(PID_t2* pid, float min, float max){
  //set output limits in PID struct
  pid->outputMin = min;
  pid->outputMax = max;
}

void PID_SetIntegralLimits2(PID_t2* pid, float min, float max){
  //set integral limits in PID struct
  pid->ITermMin = min;
  pid->ITermMax = max;

}

void PID_SetUpdateRate2(PID_t2* pid, int updateRate){
  //set integral limits in PID struct
  pid->updateRate = updateRate;
}





