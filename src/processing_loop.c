// Processing loop (contains geofence code as an example only)
// processing_loop():  Top-level thread function
// update_set_points():  Geofencing reference points (may be useful as a guide)
// processing_loop_initialize():  Set up data processing / geofence variables
// read_config:  Read configuratxion parameters from a file (customize please)
//
#define EXTERN extern
#define PROC_FREQ 100 // in Hz

#include "../include/quadcopter_main.h"
void read_config(char *);
void command_servo(int close);
void read_servo();
int processing_loop_initialize();
/*
 * State estimation and guidance thread 
*/
void *processing_loop(void *data){
  int hz = PROC_FREQ;
  processing_loop_initialize();

  // Local copies
  struct motion_capture_obs *mcobs[2];  // [0] = new, [1] = old
  struct state localstate;
  int local_fence_switch;
  int local_T_PWM;
  int machine_state = 1;    // 0 for hovering test; 1 for perching
  double pos_threshold = 0.05; // the unit is meter
  double torqueThresh = 1.0;
  double close_angle = 150.0;
  double open_angle = 240.0;

  while (1) {  // Main thread loop

    pthread_mutex_lock(&mcap_mutex);
    mcobs[0] = mcap_obs;
    if(mcap_obs[1].time > 0) mcobs[1] = mcap_obs+1;
    else               mcobs[1] = mcap_obs;
    pthread_mutex_unlock(&mcap_mutex);
  
    localstate.time = mcobs[0]->time;
    localstate.pose[0] = mcobs[0]->pose[0]; // x position
    localstate.pose[1] = mcobs[0]->pose[1]; // y position
    localstate.pose[2] = mcobs[0]->pose[2]; // z position / altitude
    localstate.pose[3] = mcobs[0]->pose[5]; // yaw angle
    localstate.pose[4] = 0;
    localstate.pose[5] = 0;
    localstate.pose[6] = 0;
    localstate.pose[7] = 0;
    
    // Estimate velocities from first-order differentiation
    double mc_time_step = mcobs[0]->time - mcobs[1]->time;
    if(mc_time_step > 1.0E-7 && mc_time_step < 1){
      localstate.pose[4] = (mcobs[0]->pose[0]-mcobs[1]->pose[0])/mc_time_step;
      localstate.pose[5] = (mcobs[0]->pose[1]-mcobs[1]->pose[1])/mc_time_step;
      localstate.pose[6] = (mcobs[0]->pose[2]-mcobs[1]->pose[2])/mc_time_step;
      localstate.pose[7] = (mcobs[0]->pose[5]-mcobs[1]->pose[5])/mc_time_step;
    }

    // Geofence activation code
    // turn fence on --> may be useful to control your autonomous controller
    pthread_mutex_lock(&fence_switch_mutex);
    local_fence_switch = fence_switch;
    local_T_PWM = T_PWM;
    pthread_mutex_unlock(&fence_switch_mutex);
    
    pthread_mutex_lock(&watchingdog_mutex);
    if (watchingdog == 1)
    {
        printf("The dog is barking!\n");
    }
    pthread_mutex_unlock(&watchingdog_mutex);
    if(localstate.fence_on == 0) 
    {
       // ADD YOUR CODE HERE
       if (local_fence_switch > 1500) 
       {
            printf("Turn Fence on\n");
            update_set_points(localstate.pose, localstate.set_points, 0); 
            localstate.fence_on = 1;
            localstate.time_fence_init = ((double)utime_now())/1000000;
       }
   
    } 
    else if(localstate.fence_on == 1)  
    {
       if (local_fence_switch < 1500)  
       {
            printf("Turn Fence off\n");
            localstate.fence_on = 0;
       }
       else
       {
            // update desired state
            int open = 0;
            int closed = 0;
            int Torque_check = 0;
            int issafe_angle = 0;
            switch (machine_state)
            {
                case 0:     // only hovering test
                    break;
                case 1: // pos 1
                    printf("CASE 1: POS 1\n");
		            update_set_points(localstate.pose, localstate.set_points, 1);
                    if (fabs(localstate.set_points[0]-localstate.pose[0]) < pos_threshold*2.0 && 
                        fabs(localstate.set_points[1]-localstate.pose[1]) < pos_threshold && 
                        fabs(localstate.set_points[2]-localstate.pose[2]) < pos_threshold) 
                    { 
                        machine_state = 2; 
                    }                    
                    break;
                case 2:     // pos before goal
                    printf("CASE 2: POS BEFORE GOAL\n");
                    update_set_points(localstate.pose, localstate.set_points, 2); 
                    if (fabs(localstate.set_points[0]-localstate.pose[0]) < pos_threshold*2.0 && 
                        fabs(localstate.set_points[1]-localstate.pose[1]) < pos_threshold/2.0 && 
                        fabs(localstate.set_points[2]-localstate.pose[2]) < pos_threshold/2.0)
                    { 
                        machine_state = 3;
                    }
                    break;
		        case 3:     //goal pos
		            printf("CASE 3: GOAL POS\n");
                    update_set_points(localstate.pose, localstate.set_points, 3); 
                    if (fabs(localstate.set_points[0]-localstate.pose[0]) < pos_threshold*2.0 && 
                        fabs(localstate.set_points[1]-localstate.pose[1]) < pos_threshold/2.0 && 
                        fabs(localstate.set_points[2]-localstate.pose[2]) < pos_threshold/2.5)
                    { 
                        machine_state = 4;
                    }    
                    break;
                case 4:     // activate grippers
                    printf("CASE 4: ACTIVATE GRIPPERS\n");
                    command_servo(1); //close
                    update_set_points(localstate.pose, localstate.set_points, 0);
                    sleep(2);
                    update_set_points(localstate.pose, localstate.set_points, 0);
                    read_servo();
                    sleep(1);
                    closed = fabs(close_angle - bus.servo[0].cur_angle) < 2 || fabs(close_angle - bus.servo[1].cur_angle) < 2 ? 1 : 0;
                    if (closed)
                    {
                        machine_state = 5;
                    }
                    break;
                case 5:     //stop rotors
                    printf("CASE 5: STOP ROTORS\n");
                    pthread_mutex_lock(&motors_mutex);
                    motors = 1; // set global variable to stop the rotors
                    pthread_mutex_unlock(&motors_mutex);
                    
                    machine_state = 6; // change !!
                    break;
                case 6: // not sure if there is a rotor off signal
                    printf("CASE 6: SAFE ANGLE AND SIGNAL TO START ROTORS\n");
                    //issafe_angle = (fabs(mcobs[0]->pose[3]) < 0.174) && (fabs(mcobs[0]->pose[4]) < 0.174) ? 1 : 0;  // less than 10 degrees
                    if (local_T_PWM > 1800)
                    {
                        machine_state = 7;
                    }
                    break;
                case 7:  // start rotors
                    printf("CASE 7: START ROTORS\n");
                    update_set_points(localstate.pose, localstate.set_points, 0); // make it hover the current position
                    
                    pthread_mutex_lock(&motors_mutex);
                    motors = 1; // set global variable to start the rotors
                    pthread_mutex_unlock(&motors_mutex);
                    
                    machine_state = 8;
                    break;
                case 8:
                    printf("CASE 8: OPEN UP\n");
                    sleep(0.5);
                    command_servo(0); //open up
                    sleep(2);
                    read_servo();
                    sleep(1);
                    open = fabs(open_angle - bus.servo[0].cur_angle) < 2 || fabs(open_angle - bus.servo[1].cur_angle) < 2 ? 1 : 0;  
                    if (open)
                    {
                        machine_state = 9;
                    }
                    break;
                case 9:
                    printf("CASE 9: HOVERING\n");
                    update_set_points(localstate.pose, localstate.set_points, 1);
                    break;
                
            }
       }
    }

    // Copy to global state (minimize total time state is locked by the mutex)
    pthread_mutex_lock(&state_mutex);
    memcpy(state, &localstate, sizeof(struct state));
    pthread_mutex_unlock(&state_mutex);

    //usleep(1000000/hz);
    usleep(10000);
  } // end while processing_loop()
  return 0;
}

/**
 * update_set_points()
 */
int update_set_points(double* pose, float *set_points, int update_setpoint_flag){
  //Add your code here to generate proper reference state for the controller
        if (update_setpoint_flag == 0) // hovering
        {       
            for (int i = 0; i < 8; i++)
            {
                    set_points[i] = pose[i];
            }
        }  
        else if (update_setpoint_flag == 1) // pos 1
        {
            set_points[0] = goal[0];
            set_points[1] = goal[1];
            set_points[2] = -0.5 + goal[2];
            set_points[3] = 0;
            for (int i = 4; i < 8; i++)
            {
                set_points[i] = 0;
            }
            
        }      
        else if (update_setpoint_flag == 2) // pos 2
        {
            set_points[0] = goal[0];
            set_points[1] = goal[1];
            set_points[2] = -0.05 + goal[2];
            set_points[3] = 0;
            for (int i = 4; i < 8; i++)
            {
                set_points[i] = 0;
            }
            
        }
        else if (update_setpoint_flag == 3) // pos goal
        {
            set_points[0] = goal[0];
            set_points[1] = goal[1];
            set_points[2] = goal[2];
            set_points[3] = 0;
            for (int i = 4; i < 8; i++)
            {
                set_points[i] = 0;
            }
            
        }
        return 0;
}


/**
 * processing_loop_initialize()
*/
int processing_loop_initialize()
{
  // Initialize state struct
  state = (state_t*) calloc(1,sizeof(*state));
  state->time = ((double)utime_now())/1000000;
  memset(state->pose,0,sizeof(state->pose));

  // Read configuration file
  char blah[] = "config.txt";
  read_config(blah);

  mcap_obs[0].time = state->time;
  mcap_obs[1].time = -1.0;  // Signal that this isn't set yet

  // Initialize Altitude Velocity Data Structures
  memset(diff_z, 0, sizeof(diff_z));
  memset(diff_z_med, 0, sizeof(diff_z_med));

  // Fence variables
  state->fence_on = 0;
  memset(state->set_points,0,sizeof(state->set_points));
  state->time_fence_init = 0;

  // Initialize IMU data
  //if(imu_mode == 'u' || imu_mode == 'r'){
  //  imu_initialize();
  //}

  return 0;
}

void read_config(char* config){
  // open configuration file
  FILE* conf = fopen(config,"r");
  // holder string
  char str[1000];

  // read in the quadrotor initial position
  fscanf(conf,"%s %lf %lf %lf",str,&goal[0], &goal[1], &goal[2]);

  // ADD YOUR CODE HERE
  fscanf(conf,"%s %f %f %f",str,&KP_pitch,&KI_pitch,&KD_pitch);
  fscanf(conf,"%s %f %f %f",str,&KP_roll,&KI_roll,&KD_roll);
  fscanf(conf,"%s %f %f %f",str,&KP_thrust,&KI_thrust,&KD_thrust);
  fscanf(conf,"%s %f %f %f",str,&KP_yaw,&KI_yaw,&KD_yaw);

  fscanf(conf,"%s %d %d %d",str,&thrust_PWM_up, &thrust_PWM_base, &thrust_PWM_down);
  fscanf(conf,"%s %d %d %d",str,&roll_PWM_left, &roll_PWM_base, &roll_PWM_right);
  fscanf(conf,"%s %d %d %d",str,&pitch_PWM_forward, &pitch_PWM_base, &pitch_PWM_backward);
  fscanf(conf,"%s %d %d %d",str,&yaw_PWM_ccw, &yaw_PWM_base, &yaw_PWM_cw);
  
  thrust_iterm = 0;
  roll_iterm = 0;
  pitch_iterm = 0;
  yaw_iterm = 0;
        
  prev_error_thrust = 0;
  prev_error_roll = 0;
  prev_error_pitch = 0;
  prev_error_yaw = 0;
    
  issetTrim = 0;
  motors = 0;
  ini_timer_motors = ((double)utime_now())/1000000;
  timer_motors = ini_timer_motors;
  
  fclose(conf);
}

void command_servo(int close)  // 1 to close
{
    float angle = 240; // open
    int i = 0;
    if (close)
    {
        angle = 150;
    }
        //printf("S: %f\n", angle);
        pthread_mutex_lock(&dynamixel_mutex);
        bus.servo[0].cmd_angle = angle; 
        bus.servo[0].cmd_speed = 0.8; 
        bus.servo[0].cmd_flag = CMD;
        bus.servo[1].cmd_angle = angle; 
        bus.servo[1].cmd_speed = 0.8; 
        bus.servo[1].cmd_flag = CMD;
        pthread_mutex_unlock(&dynamixel_mutex);
    
    
    
    return;
}

void read_servo()
{
	printf("Request Status:\n");
	pthread_mutex_lock(&dynamixel_mutex);
	bus.servo[0].cmd_flag = STATUS;
	bus.servo[1].cmd_flag = STATUS;
	pthread_mutex_unlock(&dynamixel_mutex);  
}
