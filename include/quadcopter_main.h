#ifndef QUADCOPTER_GLOBALS
#define QUADCOPTER_GLOBALS

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <unistd.h>
#include <pthread.h>

#ifdef __cplusplus
extern "C" {
#endif

//Use this in your code:  #define EXTERN extern

#include <lcm/lcm.h>
#include "../include/lcmtypes/lcmtypes_c/channels_t.h"
#include "../include/quadcopter_struct.h"
#include "../include/bbblib/bbb.h"

// Primary threads
#include "../include/run_imu.h"
#include "../include/run_motion_capture.h"
#include "../include/util.h"

/* Global Variables with mutexes for sharing */

EXTERN imu_t imudata;
EXTERN struct motion_capture_obs mcap_obs[2];
EXTERN state_t *state;
EXTERN DynamBus bus;
EXTERN int fence_switch; 
EXTERN int T_PWM; 
EXTERN int watchingdog; 
EXTERN int motors;

/* Mutexes */
EXTERN pthread_mutex_t imu_mutex; 
EXTERN pthread_mutex_t mcap_mutex;  
EXTERN pthread_mutex_t state_mutex;
EXTERN pthread_mutex_t dynamixel_mutex;
EXTERN pthread_mutex_t fence_switch_mutex;
EXTERN pthread_mutex_t watchingdog_mutex;
EXTERN pthread_mutex_t motors_mutex;

/* Global variables that are not used in multiple threads (no mutex use) */
EXTERN FILE *mcap_txt, *block_txt, *imu_txt; // Output data files

EXTERN int imu_mode, mcap_mode;
EXTERN struct imu_data *imu; 
EXTERN DynamSetup dynam; 
// PWM signal limits and neutral (baseline) settings
EXTERN int thrust_PWM_up; // Upper saturation PWM limit.
EXTERN int thrust_PWM_base; // Zero z_vela PWM base value. 
EXTERN int thrust_PWM_down; // Lower saturation PWM limit. 
EXTERN int roll_PWM_left;  // Left saturation PWM limit.
EXTERN int roll_PWM_base;
EXTERN int roll_PWM_right; //Right saturation PWM limit. 
EXTERN int pitch_PWM_forward;  // Forward direction saturation PWM limit.
EXTERN int pitch_PWM_base; // Zero pitch_dot PWM base value. 
EXTERN int pitch_PWM_backward; // Backward direction saturation PWM limit. 
EXTERN int yaw_PWM_ccw; // Counter-Clockwise saturation PWM limit (ccw = yaw left).
EXTERN int yaw_PWM_base; // Zero yaw_dot PWM base value. 
EXTERN int yaw_PWM_cw; // Clockwise saturation PWM limit (cw = yaw right). 
EXTERN int issetTrim; 
EXTERN double ini_timer_motors;
EXTERN double timer_motors;
EXTERN int d_flag;

EXTERN float thrust_iterm;
EXTERN float roll_iterm;
EXTERN float pitch_iterm;
EXTERN float yaw_iterm;
EXTERN float prev_error_thrust;
EXTERN float prev_error_roll;
EXTERN float prev_error_pitch;
EXTERN float prev_error_yaw;
EXTERN float Desire_angle;
EXTERN int servo_flag;

// Top-level thread function declarations
void *lcm_thread_loop(void *);
void *processing_loop(void *);
void *run_imu(void *);
void *run_motion_capture(void *);

/* Function declarations needed for multiple files */
void channels_handler(const lcm_recv_buf_t *rbuf, const char *channel,
		      const channels_t *msg, void *userdata);
void auto_control(float *pose, float *prevPose, float *set_points, int16_t *channels_ptr);
void auto_control2(float *pose, float *set_points, int16_t *channels_ptr);

//////////////////////////////////////////
///  Geofence stuff for testing

#define NUM_SAMPLES_MED_ALT 10
#define NUM_SAMPLES_AVG_ALT 20

EXTERN float KP_thrust, KI_thrust, KD_thrust;
EXTERN float KP_pitch, KI_pitch, KD_pitch;
EXTERN float KP_roll, KI_roll, KD_roll;
EXTERN float KP_yaw, KI_yaw, KD_yaw;

EXTERN double diff_z[NUM_SAMPLES_MED_ALT], diff_z_med[NUM_SAMPLES_AVG_ALT];
//EXTERN double start[2];
EXTERN double goal[3];
EXTERN double ang_buf;
EXTERN double fence_penalty_length;

// max desired velocity for fence corrections
EXTERN float max_vel, max_step;

EXTERN double alt_low_fence, alt_high_fence;
EXTERN double x_pos_fence, x_neg_fence, y_pos_fence, y_neg_fence;
EXTERN double alt_prev;

//////////////////////////////////////////////////////////////////////////////
// Geofence function
int update_set_points(double* pose, float* set_points, int first_time);

#ifdef __cplusplus
}
#endif

#endif
