#ifndef PID2_H
#define PID2_H

typedef struct pid2 PID_t2;

struct pid2{

  float kp; // Proportional Gain
  float ki; // Integral Gain
  float kd; // Derivative Gain
  float kd2; // Derivative Gain for auto control

  float pidInput;  // input to pid
  float pidOutput; // pid output
  float pidSetpoint; // pid setpoint
  float trim;
  float velocity;
  
  float ITerm;   // integral term
  float prevInput; // previous input for calculating derivative

  float ITermMin, ITermMax; // integral saturation
  float outputMin, outputMax; //pid output saturation
  
  int updateRate; //time in microseconds between pid loop update 
};


//Initialize the PID controller with gains
PID_t2* PID_Init2(
  float kp,
  float ki,
  float kd, 
  float trim
  );
     
void PID_Compute2(PID_t2* pid, int flag);

void PID_SetTunings2(PID_t2* pid, 
  float kp, 
  float ki, 
  float kd
  );

void PID_SetOutputLimits2(PID_t2* pid, 
  float min, 
  float max
  );

void PID_SetIntegralLimits2(PID_t2* pid, 
  float min, 
  float max
  );

void PID_SetUpdateRate2(PID_t2* pid, 
  int updateRate
  );

#endif

